<?php 
/*
 * available variables
 * $list: rendered list of thumbs of gallery (jcarousel if installed or custom list)
 * $npgallery an array of existed images in gallery
 * $main_image $image with priority = 1
 */
?>
<?php //pr($variables)?>
<script type="text/javascript">
<!--

//-->
</script>
<div class="npgallery">
	<div class="pic-prev" onclick="NPGALLERY.scroll(this)"><div class="btn-prev" ></div></div>
	
	<div class="npgallery-main-image">
		<img src="<?php echo NPGALLERY_IMAGE_STORE_URL . '/medium/' . $main_image->name?>" />
	</div>
	<div class="pic-next" onclick="NPGALLERY.scroll(this)"><div class="btn-next"></div></div>
	<div class="npgallery-all-images"><?php print $list?></div>
</div>