(function($) {
	Drupal.behaviors.npgallery = {
		attach: function (context, settings) {
			pluploader = $('.plupload-element').pluploadQueue();
			var defaultSettings = settings.npgallery['default'] ? settings.npgallery['default'] : {};
			pluploader.bind('FileUploaded', function(up, file, info) {
				var response = eval('(' + info.response + ')');
				if (response.result) {
					
					if (parseInt(response.nid) == 0) {
						$('#existed_images').append('<li><img src="' + defaultSettings.npstorage + '/pic/' + response.file_name + '"/></li>');
						$('#edit-npgallery-images > div').append('<input name="npgallery_images[]" type="hidden" value="' + response.image_id + '" />');
					}else {
						$('#existed_images').append('<li id="sort-' + response.image_id + '"><img src="' + defaultSettings.npstorage + '/pic/' + response.file_name + '"/></li>');
					}
				}
				if (up.total.uploaded == up.files.length) {
					up.init();
				}
			});
			$('.npgallery-sortable').sortable({
					helper: "clone",
					stop: function(event, ui) {
						ui.item.css({'top':'0','left':'0'});
//						var element = ui.item.attr('id');
					    var order = jQuery('.npgallery-sortable').sortable('toArray');
					    console.log(order);
						var url = '/npgallery/' + parseInt(defaultSettings.nid) + '/sort';
						jQuery.post(url, {'order': order}, function(data){});
					}
			});
			$('#existed_images img').live('click', function(){
				if (confirm('Delete this pic?')) {
					$.ajax({
						url: '/npgallery/' + this.id + '/delete-image',
						data: {'nid': parseInt(defaultSettings.nid)},
						type: 'post',
						dataType: 'json',
						success: function(data) {
							if (data.result) {
								$(this).parent('li').remove();
							}else {
								alert(data.message);
							}
						}
						
					});
				}
			});
		}
	}
})(jQuery);
