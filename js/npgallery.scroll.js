NPGALLERY = {
		getMainImage: function() {
			return jQuery('.npgallery-main-image img');
		},
		getJcarousel: function() {
			return jQuery('.jcarousel').data('jcarousel');
		},
		loadImage: function(image) {
			NPGALLERY.getMainImage().attr('src', NPGALLERY_IMAGE_STORE_URL + '/medium/' + image.id);
			jQuery('.jcarousel img').each(function() {
				jQuery(this).removeClass('npgallery-active');
			});
			jQuery(image).addClass('npgallery-active');
		},
		scroll: function(button) {
			var carousel = NPGALLERY.getJcarousel();
			var current = jQuery('.npgallery-active').parent('li');
			var next = jQuery(button).hasClass('pic-prev') ? current.prev('li').children('img') : current.next('li').children('img');
			if (next.length > 0) {
				NPGALLERY.loadImage(next[0]);
				var index = next.parent('li').attr('jcarouselindex');
				console.log(index);
				console.log(carousel.last);
				if (parseInt(index) > carousel.last) {
					jQuery('.jcarousel').jcarousel('next');
					return;
				}
				if (carousel.last > parseInt(index) && jQuery(button).hasClass('pic-prev') && (parseInt(index)%carousel.options.visible) == 0) {
					jQuery('.jcarousel').jcarousel('prev');
				}
			}
		}
}